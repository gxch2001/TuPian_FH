#TuPian_FH

#一个图片加载控件
通过BitmapUtils加载普通图片,HttpUtils下载Gif图片<br/>
实现长图截断,实时下载进度展示,多图依次加载,点击图片显示详情 
##
请在你的XML中配置

        <name.fuhan.tupian_fh.MyImageView
            android:orientation="vertical"
            android:id="@+id/liebiao_item_imageview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:layout_gravity="center_horizontal"
            android:layout_marginTop="10.0dip" />
##
并在功能清单中添加

        <activity android:name="name.fuhan.tupian_fh.ShowImageActivity" />
##
可为本控件设置默认属性

	Config.setBitmapUtils(bitmapUtils).setHttpUtils(httpUtils).setMaxHeight(maxHeight).设置默认缩放(sf);

bitmapUtils:你的应用使用的BitmapUtils,不设置时由本控件自行创建

httpUtils:你的应用使用的HttpUtils,不设置时由本控件自行创建

maxHeight:当缩放启用时图片的最大高度,默认为屏幕高度,你也可以为每个控件单独设置

sf:是否开启缩放,默认开启,你也可以为每个控件单独设置

##

在java中获取到该控件并设置参数
	
	tupianView.setAddress(address, address2, isGif,width,height);

address:缩略图地址,如果没有可填null

address2:清晰图片地址,不可空

isGif:布尔值,是否为Gif图片

width,height:图片的宽和高,在图片下载完成之前占位用的,不可为空

##
当需要多张图片依次加载时

	tupianView.setAddress(address, address2, isGif,width,height,myImageView);

myImageView:下一个待加载的MyImageView对象,当全部图片添加完后需调用最后一个MyImageView的theEnd()方法


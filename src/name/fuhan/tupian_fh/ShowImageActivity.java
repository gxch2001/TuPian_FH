package name.fuhan.tupian_fh;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.Header;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ant.liao.GifView;
import com.ant.liao.GifView.GifImageType;

public class ShowImageActivity extends Activity implements OnClickListener {

	private View fanhuiView;
	private View fenxiangView;
	private View baocunView;
	private LinearLayout neirongView;

	private GifImageView network_gifimageview;
//	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_image);

		fanhuiView = findViewById(R.id.activity_showimage_fanhui);
		fanhuiView.setOnClickListener(this);
		fenxiangView = findViewById(R.id.activity_showimage_fenxiang);
		fenxiangView.setOnClickListener(this);
		baocunView = findViewById(R.id.activity_showimage_baocun);
		baocunView.setOnClickListener(this);
		neirongView = (LinearLayout) findViewById(R.id.activity_showimage_neirong);
		Intent intent = getIntent();
		boolean isGif = intent.getBooleanExtra("isGif", false);
		String address = intent.getStringExtra("address");
		String width = intent.getStringExtra("width");
		String height = intent.getStringExtra("height");
		neirongView.addView( new MyImageView(this, false){
			public void onClick(View v) {};
		}.setAddress(null, address, isGif, width, height));
		neirongView.postInvalidate();
	}

	@Override
	public void onClick(View v) {
		if (v == fanhuiView) {
			finish();
		}
		if (v == fenxiangView) {
			// TODO 分享
		}
		if (v == baocunView) {
			// TODO 保存
		}

	}
}
